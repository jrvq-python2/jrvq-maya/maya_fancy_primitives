# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: maya_geo_methods.py
Date: 2019.02.09
Revision: 2020.01.12
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


from maya import cmds


def get_geometry(geo_type='sphere', sx=8, sy=8, sz=0):

    the_primitive = 'Fancy_geo_primitive'
    output_edges = []

    if geo_type == 'sphere':
         cmds.polySphere(name= the_primitive, sx=sx, sy=sy)
         cmds.rotate(10,0,20, the_primitive)

    elif geo_type == 'cube':
        cmds.polyCube(name=the_primitive,sx=sx, sy=sy, sz=sz)
        cmds.rotate(30, 0, 10, the_primitive)

    elif geo_type == 'cylinder':
        cmds.polyCylinder(name=the_primitive,sx=sx, sy=sy, sz=sz)
        cmds.rotate(10, 0, 20, the_primitive)

    elif geo_type == 'cone':
        cmds.polyCone(name=the_primitive,sx=sx, sy=sy, sz=sz)
        cmds.move(0, -0.2, 0, the_primitive)
        cmds.rotate(170, 0, 10, the_primitive)

    elif geo_type == 'torus':
         cmds.polyTorus(name= the_primitive, sx=sx, sy=sy)
         cmds.rotate(30,0,40, the_primitive)

    elif geo_type == 'plane':
         cmds.polyPlane(name= the_primitive, sx=sx, sy=sy)
         cmds.rotate(30,0,45, the_primitive)

    edge_count = cmds.polyEvaluate(the_primitive)['edge']
    for i in range(edge_count):
        all_edges = cmds.polyListComponentConversion('{0}.e[{1}]'.format(the_primitive, i), tv=True)
        edge_start_end = cmds.ls(all_edges, flatten=True)
        output_edges.append([cmds.pointPosition(edge_start_end[0]), cmds.pointPosition(edge_start_end[1])])

    cmds.delete(the_primitive)

    return output_edges


def create_geometry(geo_type, sx, sy, sz=0):

    if geo_type == 'sphere':
         cmds.polySphere(sx=sx, sy=sy)

    elif geo_type == 'cube':
        cmds.polyCube(sx=sx, sy=sy, sz=sz)

    elif geo_type == 'cylinder':
        cmds.polyCylinder(sx=sx, sy=sy, sz=sz)

    elif geo_type == 'cone':
        cmds.polyCone(sx=sx, sy=sy, sz=sz)

    elif geo_type == 'torus':
         cmds.polyTorus(sx=sx, sy=sy)

    elif geo_type == 'plane':
         cmds.polyPlane(sx=sx, sy=sy)